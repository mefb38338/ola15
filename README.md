# OLA15

After I submitted the assignment, I realised there were some small improvements to the code. They can be found in the *improved* branch. The main improvment is moving the global variables outside the blink function.

These changes failed, since python is a retarded language that doesn't allow you to use variables in the global scope elsewhere in other scopes.

Additionally, it doesn't allow you to use ! to invert the logic of statements. 

In conclusion, due to there seemingly being no other solution to the variable-assigning issue, updating the report and the main-line code with the changes made in the improved branch are not worthwhile. 
