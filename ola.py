from machine import Pin
import time

def initHW():
    global led1, led2, btn1, btn2, led_onboard # Declaring variables as global inside a function (disgusting). Maybe there's a better way, but I tried doing it the usual ways, and python didn't like them.
    # Below, assigning values as per instructions.
    led1 = machine.Pin(14, machine.Pin.OUT)
    led2 = machine.Pin(15, machine.Pin.OUT)
    btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
    btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)
    led_onboard = machine.Pin(25, machine.Pin.OUT)

def blink(led, times): # Generalised blink function, that takes the pin object (led) and the amount of blinks (times) as parameters.
    while times != 0: # As long as there are times left to blink, do below.
        led.value(1) # Turn led on.
        time.sleep(0.1) # Keep it on for 0.1 seconds.
        led.value(0) # Turn led off.
        time.sleep(0.1) # Necessary sleep in order for it to blink when times is greater than 1. 
        times -= 1 # Decrease times by 1 since one blink has just finished.

initHW()
while(True):
    if btn1.value() == 0: # The button disrupts the signal to the pin, so check if the signal is 0.
        blink(led1, 1)
        blink(led_onboard, 1)
    elif btn2.value() == 0: # If above statement is not true, run this if true.
        blink(led2, 1)
        blink(led_onboard, 2)
